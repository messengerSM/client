#include "startwindow.h"
#include <ctype.h>
#include <iostream>
#include <QDebug>

StartWindow::StartWindow(MySocket *sObj, MainWindow &mobj, QWidget *perent):QMainWindow(perent),mainObj(&mobj),sockObj(sObj){

    this->setFixedSize(600,500);
    QPixmap bkgnd(":/icons/res/bkg.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    connectButton = new QPushButton("CONNECT",this);
    connectButton->setGeometry(230,280,100,30);
    okButton = new QPushButton("Ok",this);
    okButton->hide();
    userName =new QTextEdit(this);
    userName->hide();
    ipAddress = new QTextEdit(this);
    ipAddress->setGeometry(200,120,200,30);
    ipAddress->setPlaceholderText("Ip address");
    port = new QTextEdit(this);
    port->setGeometry(200,160,200,30);
    port->setPlaceholderText("Port number");
    connect(connectButton,SIGNAL(clicked()), this,SLOT(connectWithServer()));
    connect(okButton,SIGNAL(clicked()), this,SLOT(openMainWindow()));


}

void StartWindow::connectWithServer(){

   bool check = sockObj->connectSock(ipAddress->toPlainText().toUtf8().constData(),port->toPlainText().toUtf8().constData());

  if(check){
      port->hide();
      ipAddress->hide();
      connectButton->hide();
      userName->show();
      okButton->show();
      userName->setGeometry(200,80,200,30);
      okButton->setGeometry(230,280,100,30);
      userName->setPlaceholderText("User name");

  }


}
void StartWindow::openMainWindow(){

    std::string name = userName->toPlainText().toUtf8().constData();
    bool space = 0;
    for(int i = 0 ; i < 20; i++){
        if(name[i] == ' '){
           userName->clear();
           userName->setPlaceholderText("Wrong user name");
           space = 1;
        }

     }
    if(name.length() == 0 || name == "onlinelist" ){
        space = 1;

    }
    if(name.length() <= 20 && space == 0){
       if(!sockObj->checkUserName(name)){
          userName->setPlaceholderText("Wrong user name");
       }
       else{
           mainObj->SockObj = sockObj;
           mainObj->userName = QString::fromStdString(name);
           mainObj->show();
           mainObj->setWindowTitle(mainObj->userName);
           mainObj->CreateThread();
           this->close();
      }
    }
    else {
        userName->clear();
        userName->setPlaceholderText("Wrong user name");
    }

}

StartWindow::~StartWindow(){
    delete connectButton;
    delete userName;
    delete okButton;
    delete ipAddress;
    delete port;



}
