#include "mainwindow.h"
#include <QApplication>
#include "startwindow.h"
#include "errorwindow.h"
#include "socket.h"
int main(int argc, char *argv[]){
    QApplication app(argc, argv);
    MySocket *mySocket = new MySocket;
    ErrorWindow errorWindow;
    MainWindow mainWindow(mySocket);
    StartWindow startWindow(mySocket,mainWindow);
    startWindow.show();

    return app.exec();
}
