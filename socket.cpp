#include "socket.h"
#include <QDebug>


MySocket::MySocket()
{
     errorObj = new ErrorWindow;
     servAddr.sin_family = AF_INET;

     sockfd = socket(AF_INET, SOCK_STREAM,0);
     if (sockfd < 0){
         errorObj->errorShow("Sochket not created");
     }

}

bool MySocket::connectSock(std::string ip, std::string port){

    hostent * to;

    if((to = gethostbyname(ip.c_str()))==NULL) {
        errorObj->errorShow("not correct ip");
        return 0;
    }

    char PORT[port.length()];
    for(unsigned int i = 0; i < port.length(); i++ ){
        PORT[i] =port[i];

    }

    servAddr.sin_addr = *(( in_addr *)to->h_addr);
    servAddr.sin_port = htons(atoi(PORT));


    int con = connect(sockfd,(sockaddr*) &servAddr, sizeof(servAddr));
    if( con < 0){
       errorObj->errorShow("Cannot connect");
       return 0;
    }
    else{
        return 1;
    }

}

void MySocket::sendMsg(std::string str){
    std::cout<<str<<std::endl;
    bzero(&pack, sizeof(Package));
    for(unsigned int i = 0; i < str.length(); i++ ){
        pack.buff[i] = str[i];
    }

    for(int i = 0; i < sendTouserName.length(); i++)
        pack.nick[i] = sendTouserName[i];

    for (int i = 0; i < sizeof(Package); i++){
          send(sockfd, (void *)((char *)&pack+i), 1, 0);
    }

}

void MySocket::setNameToSend(std::string str)
{
    sendTouserName = str;
}



bool MySocket::checkUserName(std::string name){

      char check;

      for (int i = 0; i < 20; i++){
         send(sockfd, (void *)(name.c_str()+i), 1,0);
      }
      recv(sockfd, (void*)&check, 1, 0);
      if(check == '0'){
           return 0;

      }
      else{
          return 1;
      }
}





MySocket::~MySocket()
{
   delete errorObj;
}

