TEMPLATE = app
TARGET = Client

QT = core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

HEADERS += \
    mainwindow.h \
    startwindow.h \
    errorwindow.h \
    socket.h

SOURCES += \
    mainwindow.cpp \
    main.cpp \
    startwindow.cpp \
    errorwindow.cpp \
    socket.cpp

RESOURCES += \
    resources.qrc
