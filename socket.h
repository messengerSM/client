#ifndef SOCKET_H
#define SOCKET_H
#include <string.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <strings.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "errorwindow.h"
#include <pthread.h>
#include <list>
#include <string>

struct Package{
    char nick[20];
    char buff[1024];

};



class MySocket{

public:
    MySocket();
    ~MySocket();

private:
    struct sockaddr_in servAddr;
    hostent *server;
    ErrorWindow * errorObj;
    struct Package pack;

public:
    int sockfd;
    std::string sendTouserName = " ";
    //Function that checks whether username is valid on server
    bool checkUserName(std::string name);
    // Function for  connecting to server.
    bool connectSock(std::string ip, std::string port);
    // Function for sending message to server
    void sendMsg(std::string str);
    //Function for setting name of receiver
    void setNameToSend(std::string str);
};

#endif // SOCKET_H
