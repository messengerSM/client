#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QPushButton>
#include <QListWidget>
#include <QTextEdit>
#include <errorwindow.h>
#include <iostream>
#include "socket.h"



class MainWindow : public QMainWindow{
    Q_OBJECT
public:
    explicit MainWindow(MySocket * mySock,QWidget *perent = 0);
    ~MainWindow();
    //Function for displaying message in messages window
    void handleMessage(std::string name, std::string msg);
    //Function for creating thread of receiving message from socket
    void CreateThread();
private:
    QPushButton *sendbutton;
    QTextEdit *text2;
    QTextEdit *text1;
    QListWidget *emojiList;
    pthread_t read_tid;
    std::list<std::string> messages;
    void setEmoji(QListWidget *obj);

public:
    QListWidget *onlineList;
    QString userName;
    MySocket *SockObj;


private slots:
    // Slot for sending message writen in writing window, thruw the Secket object
    void sendMsg();
    //Slot for setting name of sender thruw QListWidget itemClicked SIGNAL
    void setNameOfSender(QListWidgetItem * item);
    //Slot for adding emoji in message thruw QListWidget itemClicked SIGNAL
    void setEmoji(QListWidgetItem * item);
};

#endif // MAINWINDOW_H
