#include "errorwindow.h"

ErrorWindow::ErrorWindow(QWidget *perent):QMainWindow(perent){
      this->setFixedSize(300,200);
      errorText = new QTextEdit(this);
      errorText->setGeometry(20,30,250,50);
      errorText->setReadOnly(true);
      okButton = new QPushButton("OK",this);
      okButton->setGeometry(130,120,50,30);

      connect(okButton,SIGNAL(clicked()), this, SLOT(close()));

}
ErrorWindow::~ErrorWindow(){
    delete errorText;
    delete okButton;

}

void ErrorWindow::errorShow(QString str)
{
    errorText->setText(str);
    this->show();
}
