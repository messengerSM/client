#include "mainwindow.h"
#include <QDebug>


void * getMsg(void *ptr)
{
    MainWindow * mwin = (MainWindow *)ptr;

    Package rec_pack;
    while(true){
        bzero(&rec_pack,sizeof(Package));
        int check;

        for(unsigned int i = 0; i < sizeof(Package); i++){
            check = recv(mwin->SockObj->sockfd,(void *)((char *)&rec_pack + i),1,0);
        }

        std::string senderName(rec_pack.nick);
        std::string senderMessage(rec_pack.buff);

        if(senderName == "onlinelist"){
           unsigned int i = 0;
            mwin->onlineList->clear();
            mwin->onlineList->addItem("Send to all");

            while(i < senderMessage.length()){
                if(senderMessage[i] == ' '){
                    std::string tmp;
                    tmp = senderMessage.substr(0,i);
                    if(tmp != mwin->userName.toUtf8().constData()){
                       mwin->onlineList->addItem(QString::fromStdString(tmp));
                    }
                    senderMessage = senderMessage.substr(i+1,senderMessage.length()-(i+1));
                    i = -1;
                }
                i++;
            }
        }else{
            if(check > 0){
               mwin->handleMessage(senderName,senderMessage);
            }
        }
    }
    pthread_exit(0);
}


MainWindow::MainWindow(MySocket * mySock, QWidget *perent):QMainWindow(perent), SockObj(mySock){
      this->setFixedSize(950,600);
      QPixmap bkgnd(":/icons/res/bkg.png");
      bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
      QPalette palette;
      palette.setBrush(QPalette::Background, bkgnd);
      this->setPalette(palette);
      sendbutton = new QPushButton("Send",this);
      sendbutton->setGeometry(560,380,110,50);
      QPalette pal = sendbutton->palette();
      pal.setColor(QPalette::Button, QColor(Qt::blue));
      sendbutton->setAutoFillBackground(true);
      sendbutton->setPalette(pal);
      sendbutton->update();
      onlineList = new QListWidget(this);
      onlineList->setGeometry(700,20,220,350);
      text2 = new QTextEdit(this);
      text2->setGeometry(30,440,640,140);
      text1 = new QTextEdit(this);
      text1->setGeometry(30,20,640,350);
      text1->setReadOnly(true);
      text2->setReadOnly(true);
      emojiList = new QListWidget(this);
      emojiList->setGeometry(700,440,220,140);
      setEmoji(emojiList);
      connect(sendbutton, SIGNAL(clicked()), this, SLOT(sendMsg()));
      connect(onlineList,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(setNameOfSender(QListWidgetItem*)));
      connect(emojiList,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(setEmoji(QListWidgetItem*)));

}

void MainWindow::setEmoji(QListWidget *emojiList){

      QListWidgetItem *item1 = new QListWidgetItem;
      item1->setTextColor(Qt::white);
      item1->setText("1");
      item1->setIcon(QPixmap(":/icons/res/smile.png"));
      item1->setFlags( Qt::ItemIsEditable | Qt::ItemIsEnabled );
      emojiList->addItem(item1);
      QListWidgetItem *item2 = new QListWidgetItem;
      item2->setTextColor(Qt::white);
      item2->setText("2");
      item2->setIcon(QPixmap(":/icons/res/sad.png"));
      item2->setFlags( Qt::ItemIsEditable | Qt::ItemIsEnabled );
      emojiList->addItem(item2);
      QListWidgetItem *item3 = new QListWidgetItem;
      item3->setTextColor(Qt::white);
      item3->setText("3");
      item3->setIcon(QPixmap(":/icons/res/kiss.png"));
      item3->setFlags( Qt::ItemIsEditable | Qt::ItemIsEnabled );
      emojiList->addItem(item3);
      QListWidgetItem *item4 = new QListWidgetItem;
      item4->setTextColor(Qt::white);
      item4->setText("4");
      item4->setIcon(QPixmap(":/icons/res/hungry.png"));
      item4->setFlags( Qt::ItemIsEditable | Qt::ItemIsEnabled );
      emojiList->addItem(item4);
      QListWidgetItem *item5 = new QListWidgetItem;
      item5->setTextColor(Qt::white);
      item5->setText("5");
      item5->setIcon(QPixmap(":/icons/res/happy.png"));
      item5->setFlags( Qt::ItemIsEditable | Qt::ItemIsEnabled );
      emojiList->addItem(item5);

      emojiList->setViewMode( QListWidget::IconMode );



}

void MainWindow::sendMsg(){
    if (SockObj->sendTouserName !=" " && text2->toPlainText().length() != 0){
     QString tmp = text2->toPlainText();
     tmp.replace(QString(":)"), QString("<img src=\":/icons/res/smile.png\" />"));
     tmp.replace(QString(":("), QString("<img src=\":/icons/res/sad.png\" />"));
     tmp.replace(QString(":D"), QString("<img src=\":/icons/res/happy.png\" />"));
     tmp.replace(QString(":P"), QString("<img src=\":/icons/res/hungry.png\" />"));
     tmp.replace(QString(":*"), QString("<img src=\":/icons/res/kiss.png\" />"));
     if(tmp.length() > 1024){
        text2->setStyleSheet("QTextEdit {color:red}");
        text2->setText(tmp + "  Message is greater than 1024 symbols ");

    }
     else{
     SockObj->sendMsg(tmp.toUtf8().constData());
     std::string name(userName.toUtf8().constData());
     std::string message = tmp.toUtf8().constData();
     handleMessage(name,message);
     text2->clear();
    }
    }
}

void MainWindow::setNameOfSender(QListWidgetItem *item)

{

    QString qstr = item->text();
    SockObj->sendTouserName = qstr.toUtf8().constData();
    text2->setReadOnly(false);
}

void MainWindow::setEmoji(QListWidgetItem *item){
    QString msg = text2->toPlainText();
    QString str = item->text();
    if(str == "1"){
        msg = msg + ":)";
    }
    if(str == "2"){
        msg = msg + ":(";
    }
    if(str == "3"){
        msg = msg + ":*";
    }
    if(str == "4"){
        msg = msg + ":P";
    }
    if(str == "5"){
        msg = msg + ":D";
    }

    text2->setText(msg);

}



MainWindow::~MainWindow(){
    delete sendbutton;
    delete onlineList;
    delete emojiList;
    delete text1;
    delete text2;


}

void MainWindow::handleMessage(std::string name, std::string msg)
{



    if(name == userName.toUtf8().constData()){
        text1->setTextColor(Qt::blue);
        std::string str(SockObj->sendTouserName);
        name += " -> " + str;
        text1->append(QString::fromStdString(name));
        text1->setTextColor(Qt::black);
        text1->append(QString::fromStdString(msg));
    }else{
       text1->setTextColor(Qt::green);
       name += " : ";
       text1->append(QString::fromStdString(name));
       text1->setTextColor(Qt::black);
       text1->append(QString::fromStdString(msg));
    }

    messages.push_front(name + msg);

}

void MainWindow::CreateThread()
{

    pthread_create(&read_tid,NULL,getMsg,this);
}
