#ifndef ERRORWINDOW_H
#define ERRORWINDOW_H
#include <QMainWindow>
#include <QPushButton>
#include <QTextEdit>

class ErrorWindow:public QMainWindow{
    Q_OBJECT
public:

    ErrorWindow(QWidget *perent = 0);
    ~ErrorWindow();

private:
    QPushButton *okButton;
    QTextEdit *errorText;
public:
    // Function for displaying error messages which is given as argument QString
    void errorShow(QString);



};

#endif // ERRORWINDOW_H
