#ifndef STARTWINDOW_H
#define STARTWINDOW_H
#include <QMainWindow>
#include "mainwindow.h"
#include <QPushButton>
#include <QTextEdit>
#include "socket.h"
class StartWindow:public QMainWindow{
    Q_OBJECT
public:
    explicit StartWindow(MySocket *sObj, MainWindow &mobj, QWidget *parent = 0);
    ~StartWindow();
private slots:
     // Function for displaying chat window
    void openMainWindow();
    // Function for connecting with server,and handling entering of ip,port and username
    void connectWithServer();

private:
    MySocket *sockObj;
    MainWindow *mainObj;
    QPushButton *connectButton;
    QPushButton *okButton;
    QTextEdit *userName;
    QTextEdit *ipAddress;
    QTextEdit *port;


};

#endif // STARTWINDOW_H
